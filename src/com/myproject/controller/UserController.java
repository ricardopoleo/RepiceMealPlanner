package com.myproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myproject.dao.UserDAO;
import com.myproject.entity.User;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserDAO userDAO;
	
	@RequestMapping("/list")
	public String listUsers(Model model) {
		
		List<User> userList = userDAO.getUsers();
		
		model.addAttribute("users", userList);
		
		return "list-users";
	}
}
