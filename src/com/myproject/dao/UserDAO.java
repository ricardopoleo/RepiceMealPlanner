package com.myproject.dao;

import java.util.List;

import com.myproject.entity.User;

public interface UserDAO {

	public List<User> getUsers();
}
