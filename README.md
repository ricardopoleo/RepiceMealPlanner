# Subtitle Manager
Basic Java Hibernate + Spring project to handle common APIs and, also, happen to upload, manage and rate, movie's subtitles (under construction).

This project will have two basic purposes:

1. To setup a get ready to use repo for me to use over time (whenever I want to build another projects, cause is timecomsuming configuring Spring + Hibernate each time that I need it and setup the basic folder creation)
2. Also, after taking that outside this, I'll end up building the "Subtitle Manager" as a proof of concept and, also, to not build the common "To Do List" or "Make your own Blog" kind of Project as a "portfolio".

I'm planning to add:

- Users
- Movies
- Ratings
- Subtitles
- Categories
- Achievements
- Social Media (Login and Ranking: based on Ratings and achievements)
- Common 3rd Party APIs like
  * Facebook
  * LinkedIn
  * CrunchBase (if you dont know this, you should ;) )
  * Google Maps
  
At any rate, I'll keep updating this periodically.

Hope you have, **the most amazing day ever my reader and friend!**
