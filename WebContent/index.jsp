<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE hmtl>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
		<title>List Users</title>
	</head>
	<body>
		
		<div class="container">
		
			<h2>Hello user!</h2>
			
			<p class="lead">If you are seeing this, it means you are checking this project before is ready for the challenge (<italic>"the challenge to impress you"</italic>). Sorry for the naked interface.</p>
			<p>What I'll do here is this:</p>
			<ol>
				<li>I'll set up basic functionalities and leave this pretty much ready for any other projects that I want to build (cause I dont wanna to set up spring + hibernate each time I need to develop a new project)</li>
				<li>After that I'll finish to build something like  "opensubtitles.org" cause, tbh, is incredible boring to develop the usual "todo list" or the "blog post project" as a way to prove that I know how to code</li>
			</ol>
			
			<p>I'm considering adding this functionalities (CRUDs/Manage/etc): </p>
			
			<ul>
				<li>Users <a href="user/list">(Read)</a></li>
				<li>Movies</li>
				<li>Ratings</li>
				<li>Subtitles</li>
				<li>Categories</li>
				<li>Acchivements</li>
				<li>Social Media (Login and Ranking)</li>
				<li>Common 3rd party APIs</li>
				<ul>
					<li>Facebook</li>
					<li>LinkedIn</li>
					<li>StackOverFlow</li>
					<li>CrunchBase (if you dont know this, you should)</li>
					<li>Google Maps</li>
				</ul>
			</ul>
		</div>

	</body>
</html>